import "bootstrap/dist/css/bootstrap.min.css";
import ContentComponent from "./components/content/ContentComponent";

import TitleComponent from "./components/title/TitleComponent";

function App() {
  return (
    <div className="container text-center mt-5">
      <div className="row justify-content-center">
        <div className="col-6">
          <TitleComponent />
          <ContentComponent />
        </div>
      </div>
    </div>
  );
}

export default App;
