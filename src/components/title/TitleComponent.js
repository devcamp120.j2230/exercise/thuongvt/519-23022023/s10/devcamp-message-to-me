import React, { Component } from "react";
import TitleImage from "./title-image/TitleImage";
import TitleText from "./title-text/TitleText";

class TitleComponent extends Component {
    render() {
        return (
            <React.Fragment>
                <TitleText />
                <TitleImage />
            </React.Fragment>
        )
    }
}

export default TitleComponent;