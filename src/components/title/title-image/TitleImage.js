
import { Col, Row } from "react-bootstrap";

import backgroundImg from "../../../assets/images/background.jpg";

function TitleImage () {
        return (
            <Row>
                <Col sm={12} md={12} xs={12} lg={12}>
                <img src={backgroundImg} alt="background" width={500} />
                </Col>
            </Row>
            )
    }

export default TitleImage;