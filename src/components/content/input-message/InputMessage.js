
import { Row, Col, Button } from "react-bootstrap";

function InputMessage (props) {

  const  onInputChangeHandler = (event) => {

        let value = event.target.value;

        console.log("Input đang được nhập");
        console.log(value);
        props.inputMesageChangHandler(value);
    }

 const  onButtonClickHandler = () => {
        console.log("Nút gửi thông điệp đã được bấm")
        props.outputMessageChangHandlerProp()
    }

    
        return (
            <>
                <Row>
                    <Col sm={12} md={12} xs={12} lg={12}>
                        <Row className="mt-3">
                            <label className="form-label">Message cho bạn 12 tháng tới:</label>
                            <input className="form-control" onChange={onInputChangeHandler} value={props.inputMessageProp} />
                        </Row>
                        <Row className="mt-3">
                            <Button color="success" onClick={onButtonClickHandler}>Gửi thông điệp</Button>
                        </Row>
                    </Col>
                </Row>
            </>
        )
    }


export default InputMessage;