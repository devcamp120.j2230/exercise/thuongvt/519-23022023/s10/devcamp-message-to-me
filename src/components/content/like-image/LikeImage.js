
import { Row, Col } from "react-bootstrap";

import likeImg from "../../../assets/images/like.png";

function LikeImage (props) {
        return (
            <Row>
                <Col sm={12} md={12} xs={12} lg={12}>
                {props.outMessageProp.map((element, index)=>{
                    return <p key={index}>
                        {element}
                    </p>
                })}
                {/* <p>{this.props.outMessageProp}</p> */}
                {/* dùng toán tử ba ngôi để hiện thị ảnh quy định về hiển thị*/}
                {props.likeDisplayProp ? <img src={likeImg} alt="like" style={{width: "100px", margin: "0 auto"}}/>: null}
                
                </Col>
            </Row>
        )
    }

export default LikeImage;