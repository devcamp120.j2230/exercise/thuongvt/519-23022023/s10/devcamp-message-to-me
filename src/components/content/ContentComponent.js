
import { useState } from "react";
import InputMessage from "./input-message/InputMessage";
import LikeImage from "./like-image/LikeImage";

function ContentComponent () {
    const [inputMessage, setInputMessage] = useState("");
    const [outMessage, setOutMessage] = useState([]);
    const [likeDisplay, setLikeDisplay] = useState(false)


    const inputMesageChangHandler = (value)=>{
        setInputMessage(value)
     }

   const  outputMessageChangHandler = ()=>{
        if(inputMessage){
            setOutMessage([...outMessage,inputMessage]);
            setLikeDisplay(true)
        }
     }

    
        return (
            <>
                <InputMessage inputMessageProp = {inputMessage} inputMesageChangHandler={inputMesageChangHandler} outputMessageChangHandlerProp ={outputMessageChangHandler}/>
                <LikeImage outMessageProp = {outMessage} likeDisplayProp ={likeDisplay}/>
            </>
        )
    }

export default ContentComponent;